liblarch (3.1.0-1) experimental; urgency=medium

  * New upstream version 3.1.0
  * d/watch: Bump to version 4 and update

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Fri, 17 Dec 2021 10:24:00 -0300

liblarch (3.0-3) experimental; urgency=medium

  * debian/rules:
      - Added -a option to xvfb-run to fix FTBFS see #573679.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Wed, 04 Nov 2015 13:48:00 -0200

liblarch (3.0-2) experimental; urgency=medium

  * New Maintainer (Closes: #755298). Thanks for Luca Falavigna for your work.
  * debian/control:
      - Added xauth and xvfb to Build:Depends field.
      - Organized alphabetically Build:Depends: field.
  * debian/examples:
      - Added line to include examples files.
  * debian/rules:
      - Changed test-args to use xvfb-run in test suite.
      - Included override_dh_compress to not compress examples files.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Mon, 21 Sep 2015 14:47:00 -0300

liblarch (3.0-1) experimental; urgency=medium

  * New upstream release.
  * debian/control:
    - Build python3-liblarch package instead of python-liblarch. Adjust
      (build-)dependencies accordingly to support Python 3 only.
    - Build-depend on python3-nose, python3-gi and gir1.2-gtk-3.0
      for testsuite support.
    - Remove XS-Testsuite field, not needed anymore with recent dpkg.
  * debian/docs:
    - README renamed to README.md in upstream tarball.
  * debian/examples:
    - Install main.py example program.
  * debian/rules:
    - Support build of Python 3 module only.
    - Run testsuite at build time.
  * debian/tests/control:
    - Reference python3-smoketest and python3-liblarch instead of
      their older Python 2 versions.
  * debian/tests/python3-smoketest:
    - Rename from python2-smoketest, and adjust to work with Python 3.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 02 Jun 2015 15:05:08 +0200

liblarch (2.1.0-3) unstable; urgency=medium

  * debian/tests/python2-smoketest:
    - Provide a simple test suite based on autopkgtest.
  * debian/control:
    - Do not provide python-liblarch-gtk transitional package anymore.
    - Build-depend against dh-python for Pybuild support.
    - Adjust Homepage field to match new URL.
    - Bump Standards-Version to 3.9.6, no changes required.
  * debian/copyright:
    - Update copyright years.
  * debian/rules:
    - Build with Pybuild.

 -- Luca Falavigna <dktrkranz@debian.org>  Fri, 01 May 2015 16:28:02 +0200

liblarch (2.1.0-2) unstable; urgency=low

  * Upload to unstable.
  * debian/control:
    - Use canonical URIs for VCS fields.
  * debian/watch:
    - Point to GitHub repository, thanks Bart Martens!

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 05 May 2013 14:22:10 +0200

liblarch (2.1.0-1) experimental; urgency=low

  * New upstream release.
  * debian/control:
    - Fix Vcs-* fields links.
    - Breaks gtg older than version 0.3.
    - Provide transitional package for python-liblarch-gtk, which has
      been merged in python-liblarch.
    - Bump Standards-Version to 3.9.4, no changes required.
  * debian/copyright:
    - Point to 1.0 copyright format URL.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 10 Nov 2012 10:31:26 +0100

liblarch (0.1.0-1) unstable; urgency=low

  * Initial release (Closes: #660142).

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 16 Feb 2012 20:52:22 +0100
